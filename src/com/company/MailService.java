package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

public class MailService<T> implements Consumer<Message<T>> {
    Map<String, List<T>> mailBox = new MailBox<>();

    public Map<String, List<T>> getMailBox() {
        return mailBox;
    }

    @Override
    public void accept(Message<T> message) {
        List<T> list = mailBox.get(message.to);
        if (list.size() == 0) {
            list = new ArrayList<T>();
        }
        list.add(message.getContent());
        mailBox.put(message.to, list);
    }
}


